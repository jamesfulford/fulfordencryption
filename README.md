# FulfordEncryption
C# Library for String Encryption.
Has:

* Abstract Encrypter (Encryption namespace)

* Engine (Engine namespace)

(Was previously Enigma repository, however conversion from Console project to a C# Library proved messy.)
